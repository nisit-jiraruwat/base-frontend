var gulp = require('gulp');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var less = require('gulp-less');
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var Server = require('karma').Server;

gulp.task('copy-js', function () {
    return gulp.src(['node_modules/react-mdl/extra/material.min.js'])
        .pipe(concat('material.js'))
        .pipe(gulp.dest('./dist/specs/'));
});

gulp.task('clean-css', function () {
    return gulp.src('dist/css/*.css', {read: false})
    .pipe(clean());
});

gulp.task('less', ['clean-css'], function () {
    var error = false;
    return gulp.src(['./src/**/*.less'])
        .pipe(less())
        .on('error', function(err) {
            error = !error;
            gutil.log('\n(CSS)Error message:' + gutil.colors.red(err.message + '\n'));
            this.emit('end');
        })
        .on('end', function() {
            if(!error) {
                gutil.log(gutil.colors.green('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Less\n'));
            }
            error = false
        })
        .pipe(concat('main.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('copy-html', function() {
    var error = false;
    return gulp.src('./src/**/*.html')
    .on('error', function(err) {
        error = !error;
        gutil.log('\n(HTML)Error message:' + gutil.colors.red(err.message + '\n'));
        this.emit('end');
    })
    .on('end', function() {
        if(!error) {
            gutil.log(gutil.colors.green('\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> HTML\n'));
        }
        error = false
    })
    .pipe(gulp.dest('./dist/'));
});

gulp.task('clean-icons', function () {
    return gulp.src('dist/icons/', {read: false})
    .pipe(clean());
});

gulp.task('copy-icons', ['clean-icons'], function() {
    return gulp.src(['./icons/**/*.svg', './icons/**/*.ico', './icons/**/*.woff'])
        .pipe(gulp.dest('./dist/icons/'));
});

gulp.task('copy-css', function () {
    return gulp.src(['node_modules/react-mdl/extra/material.min.css'])
        .pipe(concat('material.css'))
        .pipe(gulp.dest('./dist/css/'));
});

/*
    *
    * Watch
    *
    */

gulp.task('watch:less', function () {
    watch('./src/**/*.less', {verbose: true}, function () {
        gulp.start('less');
    });
});

gulp.task('watch:html', function () {
    watch('./src/**/*.html', {verbose: true}, function () {
        gulp.start('copy-html');
    });
});


/*
    *
    * Test
    *
    */

gulp.task('test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('watch:test', function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js'
    }, done).start();
});

/*
    * clean
    */

/*
    * build
    */

gulp.task('watch', ['less', 'copy-icons', 'watch:less']);

gulp.task('build', ['less', 'copy-icons']);

gulp.task('default', ['less', 'copy-icons']);
