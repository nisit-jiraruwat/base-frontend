My Front-end
========

This is Project basic Login and Sign Up on web browser.
It is wrote by React.

Installation
------------
Clone project:
``` bash
$ git clone git@gitlab.com:nisit-jiraruwat/base-frontend.git
```

Install npm:

``` bash
$ npm install
```


Build javascript and index.html by webpack for develop (It will source map on browser):
``` bash
$ webpack
```
Build javascript and index.html by webpack for production (It will optimize javascript):
``` bash
$ webpack --config webpack.production.config
```

Build less file to css file by gulp:
``` bash
$ gulp
```

NGINX
------------
```nginx
server {
        listen 80;
        charset utf-8;
        index index.html;
        root /full-path/base-frontend/dist;
        server_name base.com;

        # Django media

        location / {
                try_files $uri $uri/ /index.html;
        }

        location /media  {
                alias    /full-path/base-backend/backend/media;
        }

        location /static {
                alias   /full-path/base-backend/backend/static;
        }

        location /css {
                alias /full-path/base-frontend/dist/css;
        }

        location /js {
                alias /full-path/base-frontend/dist/js;
        }

        location /queuez {
                alias /full-path/base-frontend/dist/queuez;
        }

        location /icons {
                alias /full-path/base-frontend/dist/icons;
        }

        location /b {
            #backend server
            proxy_pass      http://localhost:81;
        }
}

```
Links
------------
Project necessary:
- [My Back-end](https://gitlab.com/nisit-jiraruwat/base-backend)
