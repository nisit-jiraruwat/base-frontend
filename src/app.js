import React from 'react'
import ReactDOM from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
injectTapEventPlugin();

//import Youtube from './application/youtube/youtube-component'
import Main from './main/main'
import SignUp from './signup/signup'
import Login from './login/login'

import {ReactorMixin} from './lib/reactor'
import {router, routes} from './lib/router'

const Router = React.createClass({
    mixins: [ReactorMixin],
    childContextTypes: {
        router: React.PropTypes.object
    },
    getChildContext() {
        return { router }
    },
    getInitialState() {
        return { content: null }
    },
    componentWillMount() {
        this.on(routes.home.activated, () => {
            this.setState({ content: <Main /> })
        })
        this.on(routes.login.activated, () => {
            this.setState({ content: <Login /> })
        })
        this.on(routes.signup.activated, () => {
            this.setState({ content: <SignUp /> })
        })
    },
    render() {
        return this.state.content
    }
})

ReactDOM.render((
    <Router />
), document.getElementById('main'), () => {
    router.start('/')
})
